package com.lifestyle.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lifestyle.pojo.Student;

@RestController
@RequestMapping("/wy")
public class FirstController {
	
	@RequestMapping("/getName")
	public String getName() {
		return "david";
	}
	
	@RequestMapping("/getInput")
	public String getInput(String name) {
		return name;
	}
	
	@RequestMapping("/getInputByName")
	public String getInputByName(@RequestParam(name="user") String name,@RequestParam(name="hhh") String age) {
		return name+age;
	}
	
	@RequestMapping("/getHttpParam")
	public String getHttpParam(HttpServletRequest httpServletRequest) {
		return httpServletRequest.getParameter("age");
	}
	
	@RequestMapping("/getStudent")
	public String getStudent(Student student) {		
		return student.getName()+student.getGrade();
	}

}

