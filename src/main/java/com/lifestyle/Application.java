package com.lifestyle;


import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	private static final Log log=LogFactory.getLog(Application.class);
	
	@Autowired
	DataSource dataSource;
	
	public static void main(String[] args) {
		log.info("LifeStyle backstage start!");
		new SpringApplicationBuilder(Application.class).web(true).run(args);
	}
	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}
	
	public void run (String ...args){
		try {
			if(args!=null) {
				log.debug(args);
			}
			log.debug("DataSource="+dataSource);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		
	}
}
